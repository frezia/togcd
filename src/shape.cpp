/*

    This is the gcode generator from image that uses genetic algorithm for optimization of path
    Copyright (C) 2019  Tadeusz Puźniakowski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/

#include "shape.h"
#include "image.h"
#include "astar.h"

#include <algorithm>
#include <forward_list>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <unordered_map>
#include <set>

#include "lodepng.h"

#define time_measurement_dt(x) {}
/*void time_measurement_dt(const std::string &s = "") {
    static auto start{std::chrono::steady_clock::now()};
    const static auto start0{std::chrono::steady_clock::now()};
    const auto end{std::chrono::steady_clock::now()};
    const std::chrono::duration<double> elapsed_seconds{end - start};
    const std::chrono::duration<double> elapsed_seconds0{end - start0};
    std::cerr << "DT[" << s << "]: " << elapsed_seconds.count() << " T: " << elapsed_seconds0.count() << std::endl; 
    start = std::chrono::steady_clock::now();
    
} */


namespace tp {
namespace shape {

void append_shape_to_shapes_with_smart_joining(std::list<shape_t> &shapes, shape_t new_shape, double tool_r, const image_t &image);


image_t draw_shape(const shape_t shape, const double radius, double depth, const int margin) {
    int width = 0, height = 0;
    // generic_position_t<double, 4> multiply = { 1.0, 1.0, 1.0, 1.0 };

    for (const auto &p : shape) {
        width = std::max(width, (int)(p[0]));
        height = std::max(height, (int)(p[0]));
    }

    image_t ret = new_image(width + margin * 2, height + margin * 2, 255);
    for (const auto &p : shape) {
        if (p[2] < 0)
            draw_circle(ret, (int)(p[0]) + margin, (int)(p[0]) + margin, radius, 255 * ((depth - p[2]) / depth));
    }

    return ret;
}




auto get_max_around = [](auto &image, auto x0, auto y0) {
    using namespace std;
    auto &[width, height, image_data] = image;
    short max_found = 0;
    for (int x = x0 - 1; x < ((int)x0 + 2); x++) {
        for (int y = y0 - 1; y < ((int)y0 + 2); y++) {
            max_found = std::max(max_found, image_data.at(((y + height) % height) * width + ((x + width) % width)));
        }
    }
    return max_found;
};
auto get_min_around = [](auto &image, auto x0, auto y0) {
    using namespace std;
    auto &[width, height, image_data] = image;
    short min_found = 255;
    for (int x = x0 - 1; x < ((int)x0 + 2); x++) {
        for (int y = y0 - 1; y < ((int)y0 + 2); y++) {
            min_found = std::min(min_found, image_data.at(((y + height) % height) * width + ((x + width) % width)));
        }
    }
    return min_found;
};

unsigned char max_on_image(const image_t &image0) {
    auto [width, height, image_data] = image0;
    return *std::max_element(image_data.begin(), image_data.end());
}

namespace image_processing_to_gcode_complete {

auto make_circle_points = [](auto tool_r) {
    std::vector<std::pair<int, int>> pts;
    auto r = tool_r;
    for (int x = -r - 1; x < r + 1; x++)
        for (int y = -r - 1; y < r + 1; y++)
            if (sqrt(x * x + y * y) <= r)
                pts.push_back({x, y});
    return pts;
};
auto draw_path_circle = [](auto &image, auto &image_consumed, auto &p, auto &circle_points) {
    auto &[width, height, imagedata] = image;
    auto &[ic_width, ic_height, ic_image_data] = image_consumed;
    std::get<2>(image).at(p[1] * width + p[0]) = 255;
    for (auto &cp : circle_points) {
        int x = p[0] + cp.first;
        int y = p[1] + cp.second;
        // we put 255 only if the z value of the path is lower than the current pixel
        if ((x >= 0) && (x < (int)width) && (y >= 0) && (y < (int)height) && (std::get<2>(image)[y * width + x] <= std::get<2>(image)[p[1] * width + p[0]])) {
            std::get<2>(image)[y * width + x] = 255;
            ic_image_data[y * width + x] = 0;
        }
    }
};
auto draw_path_using_circle = [](auto &image, auto &image_consumed, auto &shape, auto &circle_points) {
    std::map<std::pair<int, int>, std::vector<std::pair<int, int>>> directions = {{{-1, 0}, {}}, {{-1, -1}, {}}, {{0, -1}, {}}, {{1, -1}, {}}, {{1, 0}, {}}, {{1, 1}, {}}, {{0, 1}, {}}, {{-1, 1}, {}}};
    for (auto &cpt : circle_points) {
        if (cpt.first == 0) {
            directions[{-1, 0}].push_back(cpt);
            directions[{1, 0}].push_back(cpt);
        }
        if (cpt.first > 0) { // ->
            directions[{1, 0}].push_back(cpt);
            directions[{1, -1}].push_back(cpt);
            directions[{1, 1}].push_back(cpt);
        }
        if (cpt.first < 0) { // ->
            directions[{-1, 0}].push_back(cpt);
            directions[{-1, -1}].push_back(cpt);
            directions[{-1, 1}].push_back(cpt);
        }
        if (cpt.second == 0) {
            directions[{0, -1}].push_back(cpt);
            directions[{0, 1}].push_back(cpt);
        }
        if (cpt.second > 0) { // ->
            directions[{0, 1}].push_back(cpt);
            directions[{-1, 1}].push_back(cpt);
            directions[{1, 1}].push_back(cpt);
        }
        if (cpt.second < 0) { // ->
            directions[{0, -1}].push_back(cpt);
            directions[{-1, -1}].push_back(cpt);
            directions[{1, -1}].push_back(cpt);
        }
        directions[{0, 0}].push_back(cpt);
    }
    point_t pprev;
    if (shape.size() > 0) {
        pprev = shape.front();
    }
    for (auto &p : shape) {
        auto dp = p - pprev;
        draw_path_circle(image, image_consumed, p, directions[{(int)dp[0], (int)dp[1]}]); // circle_points);
        pprev = p;
    }
};

} // namespace image_processing_to_gcode_complete

image_t find_edges(const image_t &image0) {
    auto &[width, height, imgdata] = image0;
    image_t ret_image = image0;
    std::get<2>(ret_image) = std::vector<short int>(imgdata.size(), -1);
    // std::vector<int> ret(imgdata.size(),-1);
    auto &ret = std::get<2>(ret_image);

    auto check_for_edge_at = [&](auto add_x, auto add_y) {
        const unsigned int lx = (add_x == -1) ? 1 : 0;
        const unsigned int rx = (add_x == 1) ? (width - 1) : width;
        const unsigned int ly = (add_y == -1) ? 1 : 0;
        const unsigned int ry = (add_y == 1) ? (height - 1) : height;
#pragma omp parallel for
        for (unsigned int y = ly; y < ry; y++) {
            for (unsigned int x = lx; x < rx; x++) {
                auto a = get_pixel(image0, x, y);
                auto b = get_pixel(image0, x + add_x, y + add_y);
                if (a < b) {
                    ret[y * width + x] = a;
                    //ret[(y + add_y) * width + (x + add_x)] = b;
                }
            }
        }
    };
    check_for_edge_at(0, 1);
    check_for_edge_at(1, 0);
    check_for_edge_at(0, -1);
    check_for_edge_at(-1, 0);

    check_for_edge_at(1, 1);
    check_for_edge_at(-1, -1);
    check_for_edge_at(1, -1);
    check_for_edge_at(-1, 1);
    time_measurement_dt(__FUNCTION__);
    return ret_image;
}

shape_t line_to_with_z(const image_t &image, point_t a, point_t b) {
    a[2] = 0.0;
    b[2] = 0.0;
    auto direction = b - a;
    auto direction_l = direction.length();
    auto direction_norm = direction * (1.0 / direction_l);
    shape_t ret;
    for (double i = 1; i < (direction_l - 1); i += 1.0) {
        auto n_p = a + direction_norm * i;
        n_p[2] = get_pixel(image, n_p[0], n_p[1]);
        ret.push_back(n_p);
    }
    return ret;
}
std::list<shape_t> generate_fill_paths_parallel(const image_t &image, double tool_r_) {
    const auto &[width, height, image_data] = image;
    std::vector<std::vector<pixel_coordinate_t>> raw_ret_list;

    int tool_r = tool_r_-0.5;
    if (tool_r < 1) tool_r = 1;
    int x = 1;
    int y = 1;
    int direction = 1;
    
    std::vector<pixel_coordinate_t> current_row;
    while (y < (int)(height - 1)) {
        
        auto z = get_pixel(image, x, y);
        if ((z < 255) && (z > 0)) {
            current_row.push_back({x, y});
        } else if (current_row.size() > 0) { // we have ready row
            raw_ret_list.push_back(current_row);
            current_row.clear();
        }
        // next row? apply it....
        x = x + direction;
        if ((x < 2) || (x >= ((int)(width -1)))) {
            direction = - direction;
            y = y + 2*tool_r;
        }
    }

    std::list<shape_t> ret;
    for (auto &row : raw_ret_list) {
        shape_t shape;
        for (auto &a : row) {
            shape.push_back({(double)(a)[0],(double)(a)[1],(double)get_pixel(image, (double)(a)[0],(double)(a)[1])});
        }
        if (shape.size() > 0 ) {
            // ret.push_back(shape);
            append_shape_to_shapes_with_smart_joining(ret, shape, tool_r*4, image);
        }
    }
    time_measurement_dt(__FUNCTION__);
    return ret;
}


shape_t find_path_between_points(tp::shape::point_t from, const tp::shape::point_t &dest_, const image_t &image, int margin = 2000) {
    auto dest = dest_;
    auto accessible = [&image](const tp::shape::point_t &p) -> tp::shape::shape_t {
        tp::shape::shape_t ret;
        const std::vector<tp::shape::pixel_coordinate_t> dpp = {{-1,0},{+1,0},{0,-1}, {0,+1},
        {-1,-1},{+1,+1},{+1,-1}, {-1,+1}};
        for (const auto dp : dpp) {
            tp::shape::pixel_coordinate_t pn = {(int)p[0], (int)p[1]};
            pn = pn + dp;
            if ((pn[0] > 1) && (pn[0] < (int)(std::get<0>(image)-1)) &&
                (pn[1] > 1) && (pn[1] < (int)(std::get<1>(image)-1))) { 
                auto z = get_pixel(image, pn[0], pn[1]);
                if ((z >= 0) && (z <= 254)) { // && (std::abs(z-p[2]) < 8)) {
                    ret.push_back({(double)pn[0],(double)pn[1], (double)z});
                }
            }
        }
        return ret;
    };
    auto  h = [](const tp::shape::point_t &a, const tp::shape::point_t &b) -> float{
        auto x = a-b;
        x[2] /= 255.0;
        //x[2] = 0.0;//(x[2] == 0)?0:1.0; // *= 2.0;
        return x.length();
    };
    auto ret = search_path_with_astar(from, dest,  accessible, h, margin);
    time_measurement_dt(__FUNCTION__);
    return ret;
}

auto for_each_pixel = [](image_t  &img, auto feedback_f) -> void {
    auto [width, height, pixels] = img;
    for (int y = 0; y < (int)height; y++) {
        for (int x = 0; x < (int)width; x++) {
            pixels[y*width+x] = feedback_f(img, x, y);
        }
    }
};


shape_t generate_and_mark_path(image_t &img, int x, int y) {
    time_measurement_dt(__FUNCTION__);
    shape_t path;
    short int pxval;
    //const short int first_color = get_pixel(img, x, y);
    auto is_valid_x_y = [&](int nnx, int nny){
        if (nnx < 0) return false;
        if (nny < 0) return false;
        if (nnx >= (int)std::get<0>(img)) return false;
        if (nny >= (int)std::get<1>(img)) return false;
        

        short int nxc = get_pixel(img, nnx, nny);
        if (std::abs(nxc - pxval) > 1) return false;
        return nxc > -1;
    };
    auto get_next_pos = [&](int x, int y) {
        //short int min_v = 512;
        for (auto [nnx, nny] : std::vector<std::pair<int,int>>{{x+1, y}, {x,y-1}, {x-1, y}, {x, y+1}}) {
            if (!is_valid_x_y(nnx,nny)) continue;
            short int nxc = get_pixel(img, nnx, nny);
            if ((nxc < 255) && (nxc >= 0)) {// && (std::abs(nxc - pxval) < 1)) { //(std::abs(nxc-first_color) < std::abs(min_v-first_color))) {
                x = nnx;
                y = nny;
                //min_v = nxc;
            }
        }
        return std::make_pair(x,y);
    };

    

    while (((pxval = get_pixel(img,x,y)) >= 0) && (pxval < 255)) {
        path.push_back({(double)x,(double)y,(double)pxval});
        set_pixel(img, x, y, -1);
        auto [ nx,ny ] = get_next_pos(x,y);
        x = nx; y = ny;
        try {
            if ((path.size() > 9) && ((path.front()-path.back()).length() < 4)) continue;
        shape_t backtrack;
        while ((get_pixel(img,x,y) < 0) && (path.size() > backtrack.size())) {
            backtrack.push_back(path.at(path.size()-1-backtrack.size()));
            int x0 = backtrack.back()[0], y0 = backtrack.back()[1];
            auto [ nx0,ny0 ] = get_next_pos(x0,y0);
            if (((pxval = get_pixel(img,nx0,ny0)) >= 0) && (pxval < 255)) {
                x = nx0; y = ny0;
                path.insert(path.end(),backtrack.begin(),backtrack.end());
                break;
            }
            if (backtrack.size() > 4) break;
        }
        } catch (std::exception &e) {

        }
    }
    
    if (path.size() > 0) {
        auto ppath = path;
        path.clear();
        for (auto p: ppath) {
            int x = p[0],y = p[1];
            auto [ nx0,ny0 ] = get_next_pos(x,y);
            if (((pxval = get_pixel(img,nx0,ny0)) >= 0) && (pxval < 255)) {
                path.push_back({(double)nx0, (double)ny0, (double)pxval});
                set_pixel(img, nx0, ny0, -1);
            } 
            path.push_back(p);
        }
    }
    time_measurement_dt(__FUNCTION__);
    return path;  
}


double shapes_distance(const shape_t &a, const shape_t &b) {
    using namespace std;
    return min(
        min((a.front()-b.front()).length(),
        (a.front()-b.back()).length()),
        min((a.back()-b.front()).length(),
        (a.back()-b.back()).length())
    );
}

void append_shape_to_shapes_with_smart_joining(std::list<shape_t> &shapes, shape_t new_shape, double connection_dist, const image_t &image) {
    const int path_finding_margin = connection_dist; 
    const int path_finding_astar_limit = connection_dist*2;
    time_measurement_dt(__FUNCTION__);
    if (shapes.size() == 0) {
        shapes.push_back(new_shape);
        return;
    }
    auto found = std::min_element(shapes.begin(), shapes.end(), [&new_shape](auto &a, auto &b){
        return shapes_distance(a,new_shape) < shapes_distance(b,new_shape);}
    );
    double distance = shapes_distance(*found, new_shape);
    // std::cerr << "min element (r=" << path_finding_margin << " )... done; " << 
    //         (*found).front() << "-" << (*found).back()  << "  -->  " << new_shape.front() << "-" << 
    //         new_shape.back() << "; distance " << distance << std::endl;
    
    if (distance < path_finding_margin) {
        shape_t connection;
        // assume we attach to the end of *found
        if (distance == (found->front()-new_shape.front()).length()) {
            if (connection.size() > 0) {
                std::reverse(found->begin(), found->end());
            } 
        } else if (distance == (found->front()-new_shape.back()).length()) {
            if (connection.size() > 0) {
                std::reverse(found->begin(), found->end());
                std::reverse(new_shape.begin(), new_shape.end());
            }
        } else if (distance == (found->back()-new_shape.back()).length()) {
            if (connection.size() > 0) {
                std::reverse(new_shape.begin(), new_shape.end());
            }
        } 
        connection = find_path_between_points(found->back(), new_shape.front(), image, path_finding_astar_limit);
        if (connection.size() > 0) {
            found->insert(found->end(), connection.begin(), connection.end());
            found->insert(found->end(), new_shape.begin(), new_shape.end());
            //std::cerr << "found connection " << connection.size() << std::endl;
        } else {
            if (shapes.size() > 1)
                shapes.insert(found, new_shape);
            else 
                shapes.push_back(new_shape);
        }
    } else {
        if (shapes.size() > 1)
            shapes.insert(found, new_shape);
        else 
            shapes.push_back(new_shape);
    }
    time_measurement_dt(__FUNCTION__);
}

std::list<shape_t> shapes_edges_to_cut(image_t edges, double tool_r, const image_t &image, std::ofstream &status_file) {
    status_file << "1.2 Follow paths" << std::endl;
    std::list<shape_t> shapes;

    auto [width, height, pixels] = edges;
    time_measurement_dt(__FUNCTION__);

    for_each_pixel(edges, [&shapes, &tool_r, &image](image_t &img, int x, int y) -> short int {
        auto path = generate_and_mark_path(img, x, y);
        if (path.size() > 0) {
            //append_shape_to_shapes_with_smart_joining(shapes, path, 32, image);
            shapes.push_back(path);
        }
        return -1;
    });

    time_measurement_dt(__FUNCTION__);
    // remove artifacts
    for (auto &shape: shapes) {
        shape_t s;
        if (shape.size() < 5) continue;
        s.push_back(shape[0]);
        for (int i = 1; i < (int)shape.size()-1; i++) {
            if (!((shape[i][2] > shape[i-1][2]) && (shape[i-1][2] == shape[i+1][2]))) {
                s.push_back(shape[i]);
            }
        }
        s.push_back(shape.back());
        shape = s;
    }
    std::reverse(shapes.begin(),shapes.end());
    //save_image(edges, "/tmp/cleared_edges.png");
    time_measurement_dt(__FUNCTION__);
    return shapes;
}

shape_t simple_smooth_path(const shape_t &path) {
    shape_t ret(path.size());
    ret[0] = path[0];
    ret.back() = path.back();
    for (size_t i = 1; i < path.size()-1; i++) {
        auto a = path[i-1];
        auto b = path[i];
        auto c = path[i+1];
        auto z = b[2];
        a[2] = 0.0;
        b[2] = 0.0;
        c[2] = 0.0;
        b = (a + b + c)*(1.0/3.0);
        b[2] = z;
        ret[i] = b;
    }
    return ret;
}

std::pair<point_t,point_t> bounding_box(const shape_t &a) {
    if (a.size() < 1) throw std::invalid_argument("shape cannot be empty!");
    point_t min_p = a[0];
    point_t max_p = a[0];
    for (size_t i = 1; i < a.size(); i ++) {
        for (size_t j = 0; j < min_p.size(); j++) min_p[j]=std::min(a[i][j],min_p[j]);
        for (size_t j = 0; j < max_p.size(); j++) max_p[j]=std::max(a[i][j],max_p[j]);
    }
    return {min_p, max_p};
}

bool is_a_shape_inside_b(const shape_t &a, const shape_t &b) {
    if((a.size() == 0) || (b.size() == 0)) return false;
    auto [a_bound_min,a_bound_max] = bounding_box(a);    
    auto [b_bound_min,b_bound_max] = bounding_box(b);
    double area_a = std::abs(a_bound_min.at(0)-a_bound_max.at(0))*std::abs(a_bound_min.at(1)-a_bound_max.at(1));
    double area_b = std::abs(b_bound_min.at(0)-b_bound_max.at(0))*std::abs(b_bound_min.at(1)-b_bound_max.at(1));
    if (area_a < area_b) {
        auto x = (a_bound_min[0]+a_bound_max[0])/2;
        auto y = (a_bound_min[1]+a_bound_max[1])/2;

        if ((x > b_bound_min[0]) && 
            (y > b_bound_min[1]) && 
            (x < b_bound_max[0]) && 
            (y < b_bound_max[1])) {
            return true;
            }
    }
    return false;
}
bool compare_shapes(const shape_t &a, const shape_t &b) {
    return is_a_shape_inside_b(a,b);
}

bool operator<(const shape_t &a, const shape_t &b) {
    return compare_shapes(a,b);
}


std::list<shape_t> fix_shapes_order(std::list<shape_t> shapes) {
    std::list<shape_t> all_shapes;
    for (auto &shape: shapes) {
        if (all_shapes.size() == 0) {all_shapes.push_back(shape);continue;}
        bool found_shape = false;
        for (auto &s: all_shapes) {
            if ( is_a_shape_inside_b(shape, s)) {
                found_shape = true;
                break;
            }
        }
        if (found_shape) all_shapes.push_front(shape);
        else all_shapes.push_back(shape);
    }

    std::list<shape_t> ret(all_shapes.begin(),all_shapes.end());
    time_measurement_dt(__FUNCTION__);
    return ret;
}

image_t reduce_colors_count(const image_t &image0, unsigned int levels) {
    if (levels <= 0 ) return image0;
    auto image = image0;
    std::vector<std::pair<unsigned char, size_t> > histogram;
    for (unsigned int c = 0; c < 256; c++) {
        histogram.push_back({c,0});
    }
    for (auto c : std::get<2>(image)) {
        histogram[c].second += 4;
        if (c < 255) histogram[c+1].second += 2;
        if (c < 254) histogram[c+2].second += 1;
    }
    histogram.erase(histogram.begin());
    std::sort(histogram.begin(), histogram.end(),[](auto &a, auto &b){return a.second < b.second;});
    std::reverse(histogram.begin(), histogram.end());
    std::vector<unsigned char> ranges;
    for (auto [c,count] :  histogram) {
        if (count > 0) {
            ranges.push_back(c);
            if (ranges.size() >= levels) break;
        }
    }
    std::sort(ranges.begin(), ranges.end());
    ranges.insert(ranges.begin(), 0);
    if (ranges.back() < 255) ranges.push_back(255);
    auto to_match_color = [&ranges](short int c) {
        int l=0, r=ranges.size();
        while ((l)<(r-1)) {
            int p = ((r-l)>>1)+l;
            if (c < ranges.at(p)) r = p;
            else if (c > ranges.at(p)) l = p;
            else return p;
        }
        return l;
    };
    for (auto &c : std::get<2>(image)) {
        auto ni = to_match_color(c);
        if (ranges[ni] < c) ni++;
        c = ranges[ni];
    }
    time_measurement_dt(__FUNCTION__);
    return image;
}

/**
 * @brief
 *
 * @param image0
 * @param tool_r
 * @param do_the_areas the radius of "tool" for the area cutting
 * @return std::list<shape_t>
 */
std::list<shape_t> image_to_shapes_2_full(const image_t &image00, double tool_r, double do_the_areas_r, std::ofstream &status_file) {
    using namespace std;
    using namespace image_processing_to_gcode_complete;
    int colors_count = 10;

    const image_t image0 = reduce_colors_count(image00, colors_count);

    if (status_file.is_open())
        status_file << "1.1 Adjust image to the tool diameter" << std::endl;
    // image that is dilated for the tool
    auto dilate_depending_on_tool_r = [&](auto im) {
        if (tool_r > 0.5)
            return image_dilate(remove_small_artifacts(im), tool_r);
        else
            return remove_small_artifacts(im);
    };
    const auto image_dilated_zero = dilate_depending_on_tool_r(image0); // first stage - fix tool size
    save_image(image_dilated_zero, "/tmp/shape_convert_dilate_o.png");

    auto edges = find_edges(image_dilated_zero);
    save_image(edges, "/tmp/shape_convert_edges.png");
    auto shapes_edges = shapes_edges_to_cut(edges, tool_r, image_dilated_zero, status_file);
    std::list<shape_t> shapes;
    for (auto &shape:shapes_edges) {
        if (shape.size() > 2) append_shape_to_shapes_with_smart_joining(shapes, shape, 5, image_dilated_zero);
    }

    if (do_the_areas_r > 0.0001) {
        auto image_details = dilate_depending_on_tool_r(image00); // first stage - fix tool size
        auto area_cutting_shapes = generate_fill_paths_parallel(image_details, do_the_areas_r);
        std::list<shape_t> nshapes;
        for (auto &acs : area_cutting_shapes) {
            append_shape_to_shapes_with_smart_joining(nshapes, acs, tool_r*4, image_dilated_zero);
        }
        shapes.insert(shapes.end(), nshapes.begin(), nshapes.end());
    }
    std::list<shape_t> ret;
    for (auto s:  shapes) {
        append_shape_to_shapes_with_smart_joining(ret, s, tool_r*8, image_dilated_zero);
    }
    ret = fix_shapes_order(ret);
    for (auto &r : ret) {
        r = simple_smooth_path(r);
    }
    return ret;
}


image_t remove_small_artifacts(const image_t image0) {
    using namespace std;
    const auto &[width, height, image0data] = image0;
    auto ret = image0;
#pragma omp parallel for
    for (int y = 0; y < (int)height; y++) {
        unsigned char a = get_pixel(ret, 0, y);
        int x0 = 0;
        for (int x = 1; x < (int)width; x++) {
            unsigned char b = get_pixel(ret, x, y);
            if (abs(a-b) < 2) {
                if ((x-x0) > 3) {
                    if (a < 128) {
                        set_pixel(ret,x,y,min(a,b));
                    } else {
                        set_pixel(ret,x,y,max(a,b));
                    }
                }
            } else {
                x0 = x;
                a = b;
            }
        }
    }
#pragma omp parallel for
    for (int y = 0; y < (int)height; y++) {
        unsigned char a = get_pixel(ret, 0, y);
        int x0 = 0;
        for (int x = 1; x < (int)width; x++) {
            unsigned char b = get_pixel(ret, width-x-1, y);
            if (abs(a-b) < 2) {
                if ((x-x0) > 3) {
                    if (a < 128) {
                        set_pixel(ret,width-x-1,y,min(a,b));
                    } else {
                        set_pixel(ret,width-x-1,y,max(a,b));
                    }
                }
            } else {
                x0 = x;
                a = b;
            }
        }
    }

    return ret;
}



} // namespace shape
} // namespace tp
