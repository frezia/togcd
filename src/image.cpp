/*

    This is the gcode generator from image that uses genetic algorithm for optimization of path
    Copyright (C) 2019  Tadeusz Puźniakowski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/

#include "image.h"
#include "lodepng.h"

#include <unordered_set>
#include <iostream>
#include <forward_list>
#include <array>
#include <list>
#include <chrono>

namespace tp {
namespace shape {


void set_pixel (image_t &image, int x, int y, const short int c) {
    auto &[width, height, imgdata] = image;
    x = ((unsigned long)x) % width;
    y = ((unsigned long)y) % height;
    imgdata[y * width + x] = c;
}
short int get_pixel (const image_t &image, int x, int y) {
    auto &[width, height, imgdata] = image;
    x = ((unsigned long)x) % width;
    y = ((unsigned long)y) % height;
    return imgdata[y * width + x];
}


image_t new_image(int width, int height, short int c) { return std::make_tuple(width, height, std::vector<short int>(width * height, c)); }
unsigned save_image(const image_t &img, const std::string &fname) {
    auto [width, height, data] = img;
    std::vector<unsigned int> tosave;
    std::vector<unsigned char> image(data.size() * 4);
    for (unsigned i = 0; i < data.size(); i++) {
        for (unsigned n = 0; n < 3; n++)
            image[i * 4 + n] = data[i];
        if (data[i] >= 0)
            image[i * 4 + 3] = 255;
        else
            image[i * 4 + 3] = 0;
    }
    return lodepng::encode(fname, image, width, height);
}
image_t load_image(const std::string &fname) {
    image_t img;
    auto &[width, height, data] = img;
    std::vector<unsigned char> out;
    unsigned error = lodepng::decode(out, width, height, fname);

    // if there's an error, display it
    if (error)
        std::cout << "decoder error " << error << ": " << lodepng_error_text(error) << std::endl;

    data.resize(width * height);
    for (size_t i = 0; i < width * height; i++)
        data[i] = out[i * 4];
    return img;
}
void draw_circle(image_t &img, const int x0, const int y0, const double r0, const unsigned char color) {
    double r2 = r0 * r0;
    for (int y = std::max(0.0, y0 - r0 - 1); y < std::min((int)std::get<1>(img), (int)(y0 + r0 + 1)); y++) {
        for (int x = std::max(0.0, x0 - r0 - 1); x < std::min((int)std::get<0>(img), (int)(x0 + r0 + 1)); x++) {
            double l = (x - x0) * (x - x0) + (y - y0) * (y - y0);
            if (l <= r2)
                std::get<2>(img)[(int)(y * std::get<0>(img)) + (int)(x)] = color;
        }
    }
}

std::vector<std::pair<int, int>> generate_ball_coordinates(const double r) {
    double d = 2.0 * r;

    auto r_0 = d / 2.0;
    int r2 = r_0 * r_0;
    std::vector<std::pair<int, int>> ballPositions;
    ballPositions.reserve(r2 * 4);
    for (int x = -r_0 - 1; x <= r_0 + 1; x++) {
        for (int y = -r_0 - 1; y <= r_0 + 1; y++) {
            int d = x * x + y * y;
            if (d <= r2) {
                ballPositions.push_back({x, y});
            }
        }
    }
    return ballPositions;
}


image_t DEPRECATED_image_dilate(const image_t image0, const double r, const std::vector<std::pair<int, int>> directionsToCheckEdge) {
    auto ballPositions = generate_ball_coordinates(r);
    const auto &[width, height, image0data] = image0;
    auto ret = image0;
    std::list<std::array<int, 3>> markeds;
    {
        const int w = width;
        const int h = height;
#pragma omp parallel for
        for (int y = 0; y < (int)h; y++) {
            for (int x = 0; x < (int)w; x++) {
                unsigned char a = get_pixel(image0, x, y);
                for (const auto &dir : directionsToCheckEdge) {
                    if (a > get_pixel(image0, (x + dir.first), (y + dir.second))) {
                        #pragma omp critical
                        {
                        markeds.push_front({x, y, a});
                        }
                    }
                }
            }
        }
    }
    if ( markeds.size() > 300000) {
        std::cout << "; calculations took to long: markeds.size() > 300000" << std::endl;
        throw std::out_of_range("calculations took to long: markeds.size() > 300000");
    }
    // 8225170 - throws
    // 225687 - works OK
    for (const auto &[x, y, a] : markeds) {
        if (a > 0) {
            
            //#pragma omp parallel for
            for (const auto &p : ballPositions)
                if ((x + p.first >= 0) && ((x + p.first) < (int)width) && (y + p.second >= 0) && ((y + p.second) < (int)height)) {
                    auto b = get_pixel(image0, x + p.first, y + p.second);
                    auto c = std::max((short int)a, (short int)b);
                    auto cpx = get_pixel(ret, x + p.first, y + p.second);
                    c = std::max((short int)cpx,(short int)c);
                    set_pixel(ret, x + p.first, y + p.second, c);
                }
        }
    }
    return ret;
}


image_t image_dilate(const image_t image0, const double r, const std::vector<std::pair<int, int>> directionsToCheckEdge) {
    using namespace std::literals; // enables literal suffixes, e.g. 24h, 1ms, 1s.
 
    const auto t_start =std::chrono::steady_clock::now();

    auto ballPositions = generate_ball_coordinates(r);
    const auto &[width, height, image0data] = image0;
    auto ret = image0;
    auto &ret_data = std::get<2>(ret);
    {
        const int w = width;
        const int h = height;

#pragma omp parallel for
        for (int y = r; y < (int)(h-r); y++) {
            for (int x = r; x < (int)(w-r); x++) {
//        for (int y = 0; y < (int)h; y++) {
//            for (int x = 0; x < (int)w; x++) {
                    short int max_v = image0data[y*w+x];
                    //#pragma omp parallel for
                    for (const auto &p : ballPositions) {
                        int x_r = x + p.first;
                        int y_r = y + p.second;
                        //if ((x_r >= 0) && (x_r < (int)width) && (y_r >= 0) && ((y_r) < (int)height)) 
                            max_v = std::max(image0data[y_r * w + x_r], max_v);
                    }
                    ret_data[y * w + x] = max_v;
                    //set_pixel(ret, x, y, max_v);
            }
        }

        for (int y = 0; y < (int)h; y=(y==(int)r)?(h-r):(y+1)) {
            for (int x = 0; x < (int)w; x=(x==(int)r)?(w-r):(x+1)) {
                    short int max_v = image0data[y*w+x];
                    // #pragma omp parallel for
                    for (const auto &p : ballPositions) {
                        int x_r = x + p.first;
                        int y_r = y + p.second;
                        if ((x_r >= 0) && (x_r < (int)width) && (y_r >= 0) && ((y_r) < (int)height)) 
                        {
                            max_v = std::max(image0data[y_r * w + x_r], max_v);
//                            max_v = std::max(get_pixel(image0, x_r, y_r), max_v);
                        }
                    }
                    set_pixel(ret, x, y, max_v);
            }
        }
    }
    const auto t_end = std::chrono::steady_clock::now();
    std::cerr
        << "dilate "
        << (t_end - t_start) / 1ms << "ms " // almost equivalent form of the above, but
        << (t_end - t_start) / 1s << "s... OK\n";  // using milliseconds and seconds accordingly
    return ret;
}


image_t flood_fill(int x0, int y0, image_t image, unsigned char c) {
    using namespace std;
    struct hash_f_t {
      inline std::size_t operator()(const std::pair<int, int> &v) const { return (v.first << 12) + v.second; }
    };
    unordered_set<pair<int, int>, hash_f_t> edge_pts = {{x0, y0}};
    unordered_set<pair<int, int>, hash_f_t> edge_pts_new;
    auto v0 = get<2>(image).at(y0 * get<0>(image) + x0);
    if ((int)c == (int)v0)
        return image;
    while (edge_pts.size() > 0) {
        for (auto e : edge_pts) {
            auto &[x, y] = e;
            x = (x + get<0>(image)) % get<0>(image);
            y = (y + get<1>(image)) % get<1>(image);
            get<2>(image).at(y * get<0>(image) + x) = c;
            if (get<2>(image).at(((y + get<1>(image) + 1) % get<1>(image)) * get<0>(image) + ((x + get<0>(image)) % get<0>(image))) == v0) {
                edge_pts_new.insert({x, y + 1});
            }
            if (get<2>(image).at(((y + get<1>(image)) % get<1>(image)) * get<0>(image) + ((x + get<0>(image) + 1) % get<0>(image))) == v0) {
                edge_pts_new.insert({x + 1, y});
            }
            if (get<2>(image).at(((y + get<1>(image) - 1) % get<1>(image)) * get<0>(image) + ((x + get<0>(image)) % get<0>(image))) == v0) {
                edge_pts_new.insert({x, y - 1});
            }
            if (get<2>(image).at(((y + get<1>(image)) % get<1>(image)) * get<0>(image) + ((x + get<0>(image) - 1) % get<0>(image))) == v0) {
                edge_pts_new.insert({x - 1, y});
            }
        }
        swap(edge_pts, edge_pts_new);
        edge_pts_new.clear();
    }

    return image;
}

bool is_image_with_black_frame(const image_t &image) {
    const auto &[width, height, image_data] = image;
    for (int x = 0; x < (int)width; x++) {
        if (get_pixel(image, x, 0) != 0)
            return false;
        if (get_pixel(image, x, height - 1) != 0)
            return false;
    }
    for (int y = 0; y < (int)height; y++) {
        if (get_pixel(image, 0, y) != 0)
            return false;
        if (get_pixel(image, width - 1, y) != 0)
            return false;
    }
    return true;
}



}
}
