/*

    This is the gcode generator from image that uses genetic algorithm for optimization of path
    Copyright (C) 2019  Tadeusz Puźniakowski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/
#ifndef __ASTAR_TP_H___2024
#define __ASTAR_TP_H___2024
#include <set>
#include <unordered_map>
#include <vector>
#include <set>
#include <map>
#include <functional>
#include <cmath>
#include <type_traits>
#include <algorithm>
#include <queue>

namespace tp {
/**
 * @brief A* algorithm implementation
 * 
 * @param start start point
 * @param goal goal point
 * @param accessible list of accessible nodes from the current node. For example std::vector<point_t> accessible ( const auto &bitmap, const auto &dims, const point_t & p )
 * @param h heuristic function like std::function < float (const point_t &,const point_t &)>
 * @param callback_f callback function for each iteration. It takes the current g_score map that contains all visited nodes
 * @param limitD the maximal depth of path, or -1 if any lenght is valid. Allows for early stopping of calculations
 * 
 * @return std::vector<std::decay_t<decltype(start)>> the path. If this is empty, then no path was found
 */
auto search_path_with_astar(auto start, const auto &goal, const auto accessible, const auto h, const int limitD=-1)  {
	using element_t = std::decay_t<decltype(start)>;
	using path_t = std::vector<element_t>;
    using node_cost_pair_t = std::pair<double, element_t>; // Pair for priority queue

	auto came_from = std::map<element_t, element_t > {};
	auto g_score = std::map<element_t, float >{{start, 0.0}};
	auto f_score = std::map<element_t, float >{{start, 0.0 + h( start, goal )}};
    auto openSet = std::priority_queue<node_cost_pair_t, std::vector<node_cost_pair_t>, std::greater<>>{};
    openSet.emplace(0.0, start);

	while ( openSet.size() > 0 ) {
		auto [best_h, best] = openSet.top();
		if ( best == goal ) {
			auto path = path_t{};
			for (auto current = goal; came_from.count( current ) > 0; current = came_from[current] )
				path.push_back( current );
			path.push_back( start );
			std::reverse(path.begin(), path.end());
			return path;
		}
		openSet.pop( );
		if ( ( limitD > 0 ) && ( f_score[best] > limitD ) ) {
			return path_t{};
		}
		for ( auto neighbor : accessible( best ) ) {
			float t_g_score = g_score[best] + h( neighbor, best );
			// we can skip closed set here
			if (!(g_score.count(neighbor) > 0) || (t_g_score < g_score[neighbor])) {
				came_from[neighbor] = best;
				g_score[neighbor] = t_g_score;
				f_score[neighbor] = g_score[neighbor] + h( neighbor, goal );
				openSet.emplace(f_score[neighbor], neighbor);
			}
		}
	}

	return path_t{};
};
}
#endif
