/*

    This is the gcode generator from image that uses genetic algorithm for optimization of path
    Copyright (C) 2019  Tadeusz Puźniakowski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/

#ifndef __SHAPE_TP_HPP___
#define __SHAPE_TP_HPP___

#include "image.h"
#include "distance_t.h"
#include <fstream>
#include <functional>
#include <future>
#include <list>
#include <unordered_map>
#include <vector>

namespace tp {
namespace shape {
//using namespace raspigcd;

// X, Y, Z, F
using shape_t = std::vector<raspigcd::generic_position_t<double, 3>>;
using shape_pt = std::shared_ptr<shape_t>;
using point_t = raspigcd::generic_position_t<double, 3>;

using pixel_coordinate_t = raspigcd::generic_position_t<int, 2>;

inline std::ostream &operator<<(std::ostream &o, const shape_t &s) {
    o << "[ ";
    for (const auto &e: s) o << e;
    o << "]";
    return o;
}

bool operator<(const shape_t &a, const shape_t &b);

/**
 * draws shape on image.
 *
 * It takes x,y, and depth
 *
 * the deeper the depth (negative values) the darker it is
 * */
image_t draw_shape(const shape_t shape, const double radius, double depth, const int margin);

std::list<shape_t> image_to_shapes_2_full(const image_t &image0, double tool_r, double do_the_areas_r, std::ofstream &status_file);
} // namespace shape
} // namespace tp


template <>
struct std::hash<tp::shape::point_t> {
	std::size_t operator()( const tp::shape::point_t& s ) const noexcept {
		std::size_t v = 0x012345;
        const auto limiter = ( sizeof( decltype( s[0] ) ) * s.size() );
        auto sd = ( unsigned char * )( s.data() );
        const auto rr = ( 8 * ( sizeof( size_t ) -1 ) ) ;
		for ( auto i = size_t{0}; i < limiter; i++ ) {
			v = (( v << 8 ) + ( v >> rr)) ^ ( ( size_t ) sd[i] );
		}
		return v;
/*        union {
            double x;
            size_t s;
        } v;
        size_t ret = 0x01287aabc;
        for (auto vv : s) {
            v.x = vv;
            auto e = (sizeof(size_t)-1)*8;
            ret = (((ret & (0x0ff << e)) >> e) | (ret << 8)) ^ v.s;
        }
        
        return ret;        */
	}
};

#endif
