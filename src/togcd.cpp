/**
 * @file main.cpp
 * @author Tadeusz Puźniakowski
 * @brief Simple PNG to gcode generator with support for tool width
 * @version 0.1
 * @date 2022-02-01
 *
 * @copyright Copyright (c) 2022
 *
 */

/*

    This is the gcode generator from image that uses genetic algorithm for optimization of path
    Copyright (C) 2019  Tadeusz Puźniakowski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/
#define __TP_ARGS_MAX_ARG_NAME_LENGTH 25
#include "shape.h"
#include <tp_args.hpp>

#include <any>
#include <fstream>
#include <iostream>
#include <map>
#include <regex>
#include <vector>

inline double dpi_to_dpmm(double p, double dpi) { return p * 25.4 / dpi; }
inline double dpmm_to_dpi(double p, double dpi) { return p * dpi / 25.4; }

int main(int argc, char **argv) {
    using namespace tp::args;

    auto help = arg(argc, argv, "help", false, "The help screen.");
    auto input_fname = arg(argc, argv, "file", std::string(""), "The input file name.");
    auto dpi = arg(argc, argv, "dpi", 300, "resolution of the source image [dpi].", 30, 1200);
    auto tool_d = arg(argc, argv, "tool_d", 2.0, "diameter of the tool [mm]", 0.0, 5.0);
    auto tool_d_areas = arg(argc, argv, "tool_d_areas", 2.0, "the thickness of areas cut or 0 if no areas [mm]", 0.0, 5.0);
    auto drill_depth = arg(argc, argv, "drill_depth", 3.0, "how deep do we need to cut the material [mm]", 0.0, 30.0);
    auto fly_z = arg(argc, argv, "fly_z", 2.0, "distance over the surface of the material for fly movez [mm]", 0.1, 20.0);
    auto g1feedrate = arg(argc, argv, "g1feedrate", 6.0, "speed of cutting [mm/s]", 0.5, 120.0);
    auto g1drillfeedrate = arg(argc, argv, "g1drillfeedrate", 0.5, "speed of drill movements [mm/s]", 0.1, 5.0);
    auto multipass = arg(argc, argv, "multipass", 1, "how many cutting iterations", 1, 100);
    auto safe_cut = arg(argc, argv, "safe_cut", 128, "how high should be the left over connection that keeps elements in material", 0, 255);
    auto status_fname = arg(argc, argv, "status_fname", std::string(""), "filename that will get the execution status updates");
    auto dp_epsilon = arg(argc, argv, "dp_epsilon", 0.12, "Ramer–Douglas–Peucker algorithm epsilon value in [mm]");

    if (help) {
        std::cout << "help screen.." << std::endl;
        args_info(std::cout);
        return 0;
    }

    if (input_fname == "") {
        std::cout << "; Error - no input file!" << std::endl;
        return 0;
    }
    std::ofstream status_file;
    if (status_fname != "")
        status_file.open(status_fname);
    if (status_file.is_open())
        status_file << "0 Opening file" << std::endl;
    auto image_loaded = tp::shape::load_image(input_fname);
    if (status_file.is_open())
        status_file << "1 Generating shapes list" << std::endl;
    int image_height = std::get<1>(image_loaded);
    auto color_to_z = [&](u_char c) { return ((double)(c - 255) / 255.0) * drill_depth; };
    auto mod_x = [&](double x) { return dpi_to_dpmm(x, dpi); };
    auto mod_y = [&](double y) { return dpi_to_dpmm(image_height - y, dpi); };
    using namespace tp::shape;

    std::list<shape_t> contours_generated = image_to_shapes_2_full(image_loaded, dpmm_to_dpi(tool_d, dpi) / 2.0, dpmm_to_dpi(tool_d_areas, dpi) / 2.0, status_file); // tool_d);

    if (status_file.is_open())
        status_file << "2 Handling safe cutting" << std::endl;

    if (safe_cut != 0) {
        double r = dpmm_to_dpi(tool_d, dpi);
        if (status_file.is_open())
            status_file << "2.1 Processing " << contours_generated.size() << " contours" << std::endl;
        /* for (auto &contour : contours_generated) {
            int maximal_idx = 0;

            for (unsigned i = 0; i < contour.size(); i++) {
                if (((contour[0] - contour[maximal_idx]).length()) > ((contour[i] - contour[maximal_idx]).length())) {
                    maximal_idx = i;
                }
            }

            for (auto &p : contour) {
                auto pX = p;
                auto a = contour[0];
                auto b = contour[maximal_idx];
                pX[2] = 0;
                a[2] = 0;
                b[2] = 0;
                if (((pX - a).length() <= r) || ((pX - b).length() <= r)) {
                    p[2] = std::max((double)safe_cut, p[2]);
                }
            }
        } */
        for (auto &contour : contours_generated) {
            std::vector<point_t> max_all;

            double min_z = 255;
            for (auto &e : contour) if (e[2] < min_z) min_z = e[2];
            if (min_z > 0.001) continue;
            max_all.push_back(*std::max_element(contour.begin(), contour.end(), [](auto a, auto b) { return a[0] < b[0]; }));
            max_all.push_back(*std::max_element(contour.begin(), contour.end(), [](auto a, auto b) { return a[1] < b[1]; }));
            max_all.push_back(*std::max_element(contour.begin(), contour.end(), [](auto a, auto b) { return a[0] > b[0]; }));
            max_all.push_back(*std::max_element(contour.begin(), contour.end(), [](auto a, auto b) { return a[1] > b[1]; }));
            for (auto &e : max_all) e[2] = 0;
            for (auto &p : contour) {
                auto a = p;
                a[2] = 0;

                std::vector<double> distances;
                for (auto &m : max_all) distances.push_back((m - a).length());
                double min_dist = *std::min_element(distances.begin(), distances.end());
                if (min_dist < r*1.8) {
                    //double rr = (double)safe_cut * (1.0 - min_dist/r*2.0);
                    p[2] = std::max((double)safe_cut, p[2]);
                }
            }
        }
    }

    if (status_file.is_open())
        status_file << "3 Optimizing " << contours_generated.size() << " contours using DP algorithm" << std::endl;
    if (dp_epsilon > 0.0) {
        for (auto &contour : contours_generated) {
            for (auto &p : contour) {
                p[0] = mod_x(p[0]);
                p[1] = mod_y(p[1]);
                p[2] = color_to_z(p[2]);
            }
            contour = optimize_path_dp(contour, dp_epsilon);//dpmm_to_dpi(dp_epsilon, dpi));
        }
    }
    std::cout << "M3"
              << "\n";
    if (status_file.is_open())
        status_file << "4 Generating G-Code commands for " << contours_generated.size() << " contours" << std::endl;
    for (auto contour : contours_generated) {
        std::cout << "G0Z" << fly_z
                  << "\n";
        std::cout << "G0X" << contour.at(0)[0] << "Y" << contour.at(0)[1] << "\n";
        std::cout << "G0Z0.1"
                  << "\n";
        double prev_z = 0.1;

        for (int i = 1; i <= multipass; i++) {
            double minimal_z = -drill_depth * i / multipass;
            int was_below_z = 0;
            for (auto p : contour) {
                double z = std::max(p[2], minimal_z);
                if (p[2] < minimal_z)
                    was_below_z = 1;
                
                if (z == prev_z) {
                    std::cout << "G1X" << p[0] << "Y" << p[1] << "\n";
                } else {
                    std::cout << "G1X" << p[0] << "Y" << p[1] << "\n";
                    std::cout << "G1Z" << z << "F" << g1drillfeedrate << "\n";
                    std::cout << "G1F" << g1feedrate << "\n";
                }

                /* std::cout << "G1X" << mod_x(p[0]) << "Y" << mod_y(p[1]);
                std::cout << "\n";
                if (z != prev_z) {
                    std::cout << "G1Z" << z << "F" << g1drillfeedrate << "\n";
                    std::cout << "G1F" << g1feedrate << "\n";
                } */
                prev_z = z;
            }
            if (was_below_z == 0)
                break;
            std::reverse(contour.begin(), contour.end());
        }
    }
    std::cout << "G0Z" << fly_z
              << "\n";
    std::cout << "G0X0Y0"
              << "\n";
    std::cout << "G0Z0"
              << "\n";
    std::cout << "M5"
              << "\n";
    if (status_file.is_open())
        status_file << "5 Finished generating gcode" << std::endl;
    return 0;
}
