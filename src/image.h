/*

    This is the gcode generator from image that uses genetic algorithm for optimization of path
    Copyright (C) 2019  Tadeusz Puźniakowski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/

#ifndef __IMAGE_H_FREZIA
#define __IMAGE_H_FREZIA
#include <tuple>
#include <string>
#include <vector>

namespace tp {
namespace shape {
// width, height, data
using image_t = std::tuple<unsigned, unsigned, std::vector<short int>>;

void set_pixel (image_t &image, int x, int y, const short int c);
short int get_pixel (const image_t &image, int x, int y);

/**
 * create new image with given resolution. Black and white
 * */
image_t new_image(int width, int height, short int c = 0);

unsigned save_image(const image_t &img, const std::string &fname);
image_t load_image(const std::string &fname);

void draw_circle(image_t &img, const int x, const int y, const double r, const unsigned char color);

/**
 * @brief dilates image multiple times until there are no additional areas to dilate
 *
 * continues until callbackf  returns true. True means continue next level.
 */
//void image_dilate_multiscale(const image_t &image, const double r, std::function<bool(image_t &image)> callbackf);

image_t image_dilate(const image_t image0, const double r, const std::vector<std::pair<int, int>> directionsToCheckEdge = {{-1, 0}, {0, -1}, {1, 0}, {0, 1}});
image_t remove_small_artifacts(const image_t image0);
image_t flood_fill(int x, int y, image_t image, unsigned char c);

std::vector<std::pair<int, int>> generate_ball_coordinates(const double r);

bool is_image_with_black_frame(const image_t &image);
}
}

#endif
